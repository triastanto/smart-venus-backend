<?php

namespace App\Http\Controllers;

use App\Models\Initializer;
use Illuminate\Http\Request;

class InitializerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Initializer  $initializer
     * @return \Illuminate\Http\Response
     */
    public function show(Initializer $initializer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Initializer  $initializer
     * @return \Illuminate\Http\Response
     */
    public function edit(Initializer $initializer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Initializer  $initializer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Initializer $initializer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Initializer  $initializer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Initializer $initializer)
    {
        //
    }
}
