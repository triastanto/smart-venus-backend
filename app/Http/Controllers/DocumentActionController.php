<?php

namespace App\Http\Controllers;

use App\Models\DocumentAction;
use Illuminate\Http\Request;

class DocumentActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentAction  $documentAction
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentAction $documentAction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentAction  $documentAction
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentAction $documentAction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentAction  $documentAction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentAction $documentAction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentAction  $documentAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentAction $documentAction)
    {
        //
    }
}
