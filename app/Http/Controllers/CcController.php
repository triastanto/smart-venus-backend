<?php

namespace App\Http\Controllers;

use App\Models\Cc;
use Illuminate\Http\Request;

class CcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cc  $cc
     * @return \Illuminate\Http\Response
     */
    public function show(Cc $cc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cc  $cc
     * @return \Illuminate\Http\Response
     */
    public function edit(Cc $cc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cc  $cc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cc $cc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cc  $cc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cc $cc)
    {
        //
    }
}
