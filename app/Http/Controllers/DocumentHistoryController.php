<?php

namespace App\Http\Controllers;

use App\Models\DocumentHistory;
use Illuminate\Http\Request;

class DocumentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentHistory  $documentHistory
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentHistory $documentHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentHistory  $documentHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentHistory $documentHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentHistory  $documentHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentHistory $documentHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentHistory  $documentHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentHistory $documentHistory)
    {
        //
    }
}
