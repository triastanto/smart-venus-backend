<?php

namespace App\Models\Traits;

use App\Models\Attachment;
use App\Models\Cc;
use App\Models\Initializer;
use App\Models\Recepient;
use App\Models\Sender;

trait DocumentRelationship
{
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function ccs()
    {
        return $this->hasMany(Cc::class);
    }

    public function initializers()
    {
        return $this->hasMany(Initializer::class);
    }

    public function recepients()
    {
        return $this->hasMany(Recepient::class);
    }

    public function sender()
    {
        return $this->hasOne(Sender::class);
    }
}