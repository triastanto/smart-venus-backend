<?php

namespace App\Models;

use App\Models\Traits\DocumentRelationship;
use Brexis\LaravelWorkflow\Traits\WorkflowTrait;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use WorkflowTrait,
        DocumentRelationship;
}
