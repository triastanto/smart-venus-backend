<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'official_date' => $faker->date(),
        'type' => $faker->randomElement(config('venus.document.type')),
        'urgency' => $faker->randomElement(config('venus.document.urgency')),
        'subject' => $faker->sentence(5),
        'body' => $faker->randomHtml(2,3),
        'stage' => $faker->randomElement(config('workflow.straight.places'))
    ];
});
