<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attachment;
use Faker\Generator as Faker;

$factory->define(Attachment::class, function (Faker $faker) {
    return [
        'name' => $faker->md5(),
        'type' => $faker->randomElement(config('venus.attachment.type')),
        'size_in_bytes' => $faker->randomNumber(),
        'original' => $faker->sentence(),
    ];
});
