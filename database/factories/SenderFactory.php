<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Sender;
use Faker\Generator as Faker;

$factory->define(Sender::class, function (Faker $faker) {
    return [
        'entity_type' => 'organization',
        'entity_id' => $faker->randomNumber(3),
        'name' => $faker->name,
    ];
});
