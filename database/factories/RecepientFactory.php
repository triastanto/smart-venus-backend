<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Recepient;
use Faker\Generator as Faker;

$factory->define(Recepient::class, function (Faker $faker) {
    return [
        'entity_type' => $faker->randomElement(config('venus.entity')),
        'entity_id' => $faker->randomNumber(3),
        'name' => $faker->name,
    ];
});
