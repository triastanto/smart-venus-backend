<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'documents',
            'attachments',
        ]);

        factory(App\Models\Document::class, 50)->create()->each(function ($document) {
            $count = $this->generateRandomCount();
            for ($i = 0; $i < $count; $i++) $document->attachments()->save(factory(App\Models\Attachment::class)->make());

            $count = $this->generateRandomCount();
            for ($i = 0; $i < $count; $i++) $document->attachments()->save(factory(App\Models\Cc::class)->make());

            $count = $this->generateRandomCount();
            for ($i = 0; $i < $count; $i++) $document->attachments()->save(factory(App\Models\Initializer::class)->make());

            $count = $this->generateRandomCount();
            for ($i = 0; $i < $count; $i++) $document->attachments()->save(factory(App\Models\Recepient::class)->make());

            $count = $this->generateRandomCount();
            for ($i = 0; $i < $count; $i++) $document->attachments()->save(factory(App\Models\Sender::class)->make());
        });

        Model::reguard();
    }

    protected function generateRandomCount()
    {
        return rand(1, 6);
    }
}
