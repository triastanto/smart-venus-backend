<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSmartvenusbackendTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('official_date');
            $table->string('type');
            $table->string('urgency');
            $table->string('subject');
            $table->text('body');
            $table->string('stage')->nullable();
            $table->timestamps();
        });

        Schema::create('document_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('stage');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::create('document_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('name');
            $table->unsignedBigInteger('submit_by');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::create('senders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('entity_type');
            $table->string('entity_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('recepients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('entity_type');
            $table->string('entity_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('ccs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('entity_type');
            $table->string('entity_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('initializers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('entity_type');
            $table->string('entity_id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_id');
            $table->string('name');
            $table->string('original');
            $table->string('type', 5);
            $table->unsignedBigInteger('size_in_bytes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
        Schema::dropIfExists('document_histories');
        Schema::dropIfExists('document_actions');
        Schema::dropIfExists('senders');
        Schema::dropIfExists('recepients');
        Schema::dropIfExists('ccs');
        Schema::dropIfExists('types');
        Schema::dropIfExists('urgencies');
        Schema::dropIfExists('initializers');
        Schema::dropIfExists('attachments');
    }
}
