<?php

return [
    'document' => [
        'type' => ['memo dinas', 'undangan', 'surat', 'edaran'],
        'urgency' => ['biasa', 'penting', 'rahasia']
    ],
    'attachment' => [
        'type' => ['pdf', 'doc', 'jpg', 'png', 'xls', 'ppt']
    ],
    'entity' => ['organizations', 'positions', 'employees'],
];