<?php

return [
    'straight'   => [
        'type'          => 'workflow', // or 'state_machine'
        'marking_store' => [
            'type'      => 'single_state', // explicitly use symfony/workflow:4.2 for support multiple_state & single_state
            'arguments' => ['stage']
        ],
        'supports'      => ['App\Models\Document'],
        'places'        => ['draft', 'initialized', 'sent', 'dispositioned', 'closed', 'rejected'],
        'transitions'   => [
            'initial' => [
                'from' => 'draft',
                'to'   => 'initialized'
            ],
            'reject_draft' => [
                'from' => 'draft',
                'to'   => 'rejected'
            ],
            'validate' => [
                'from' => 'initialized',
                'to'   => 'sent'
            ],
            'reject_initialized' => [
                'from' => 'initialized',
                'to'   => 'rejected',
            ],
            'dispose' => [
                'from' => 'sent',
                'to'   => 'dispositioned',
            ],
            'close' => [
                'from' => 'sent',
                'to'   => 'closed',
            ],
        ],
    ]
];